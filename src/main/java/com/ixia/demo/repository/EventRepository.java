package com.ixia.demo.repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;

import com.ixia.demo.model.Event;

@Repository
public class EventRepository {

	private List<Event> events;

	@PostConstruct
	public void initEvents() {
		try {
			events = new ArrayList<>();

			ClassPathResource resource = new ClassPathResource("/events.csv");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			String line = bufferedReader.readLine();
			while ((line = bufferedReader.readLine()) != null) {
				String[] values = line.split(",");
				Event event = new Event();
				event.setId(values[0]);
				event.setName(values[1]);
				event.setHost(values[2]);
				event.setStartTime(values[3]);
				event.setEndTime(values[4]);
				event.setCategory(values[5]);
				events.add(event);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Event> getEventsByCategory(String category) {
		return events.stream().filter(p -> p.getCategory().equals(category)).collect(Collectors.toList());
	}

	public List<String> getHost() {
		return events.stream().map(p->p.getHost()).collect(Collectors.toList());
	}
}
