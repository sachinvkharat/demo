package com.ixia.demo.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.ixia.demo.model.Event;
import com.ixia.demo.repository.EventRepository;

@Path("/event")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EventService {

	@Autowired
	private EventRepository eventRepository;

	@GET
	@Path("/events")
	public List<Event> findEventsByCategory(@QueryParam("category")String category) {
		List<Event> list = eventRepository.getEventsByCategory(category);
		return list;
	}

	@GET
	@Path("/hosts")
	public List<String> findHosts() {
		List<String> hosts = eventRepository.getHost();
		return hosts;
	}
}
