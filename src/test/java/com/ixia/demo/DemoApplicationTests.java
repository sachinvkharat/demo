package com.ixia.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	private static final String HOST_SERVICE_URL = "http://localhost:8080/event/hosts";

	private static final String CATEGORY_SERVICE_URL = "http://localhost:8080/event/events?category=Orientation";

	@Test
	public void findHosts_thenResponseCodeSuccess() throws IOException {
		final HttpUriRequest request = new HttpGet(HOST_SERVICE_URL);

		final HttpResponse httpResponse = (HttpResponse) HttpClientBuilder.create().build().execute(request);
		assertEquals(httpResponse.getStatusLine().getStatusCode(), 200);
	}

	@Test
	public void findEventsByCategory_thenResponseCodeSuccess() throws IOException {
		final HttpUriRequest request = new HttpGet(CATEGORY_SERVICE_URL);

		final HttpResponse httpResponse = (HttpResponse) HttpClientBuilder.create().build().execute(request);
		assertEquals(httpResponse.getStatusLine().getStatusCode(), 200);
	}
}
